### PHP _ MYSQL

#### Get Started

- Clone this project by copying the following to your terminal ``` git clone git@bitbucket.org:garethw1994/todo-app.git ``` and run or click the clone button in the top-right corner.

- Before started up the application first set up the database and necessary table in MYSQL by running the following command in the root of this project: 
  ``` mysql -u root -p < CREATE_DB_TABLE.sql ```

- Then start up a local server by running the following command: ``` php -S localhost:8888 ``` (Note: you can change the port number however you see fit).

- Go to `` http://localhost:8888 `` to run application;