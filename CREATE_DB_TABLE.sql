DROP DATABASE mytodos;

CREATE DATABASE mytodos;

USE mytodos;

CREATE TABLE todos (description text NOT NULL, completed boolean NOT NULL);